'use strict';

let matches = require('matches-selector');

export default class Closest {
	static check(item, target, checkYourSelf = false) {
		let parent = checkYourSelf ? item : item.parentNode;
		
		while (parent && parent !== document) {
			if (parent === target || (typeof target !== 'object' && matches(parent, target))) {
				return parent;
			}
			parent = parent.parentNode;
		}
	}
}