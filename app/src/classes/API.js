'use strict';

import Ajax from './Ajax';

import store from '../resources/store';

function request(endpoint, type, data, options = {}) {
	return new Promise((resolve, reject) => {
		new Ajax(Object.assign({}, {
			url: API_URL + endpoint,
			type: type,
			data: data,
			dataType: 'json'
		}, options))
			.then(response => {
				if(response.status === 200) {
					resolve(response)
				}   else {
					reject(response)
				}
			})
			.catch(response => {
				reject(response);
			});
	});
}

export default class API {
	static processQuery(model) {
		return new Promise((resolve, reject) => {
			request('addresses', 'get', model)
				.then(response => {
					resolve(response.body);
				})
				.catch(response => {
					reject(response);
				});
		})
	}
}
