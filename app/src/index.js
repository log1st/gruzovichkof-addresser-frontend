'use strict';

import "./styles/index.scss";

import Vue from 'vue';
import store from './resources/store';
import router from './resources/router';
import i18n from './resources/i18n';
import Body from './templates/Body/Body.vue';

import moment from 'moment';

moment.locale('ru');

let VueTouch = require('vue-touch');
Vue.use(VueTouch, {
	name: 'v-touch'
});

[]
	.map(item => {
		Vue.component('c-' + item.toLowerCase(), require('./templates/Components/' + item + '/' + item + '.vue').default);
	});

[]
	.map(item => {
		Vue.component('p-' + item.toLowerCase(), require('./templates/Partials/' + item + '/' + item + '.vue').default);
	});

['Text']
	.map(item => {
		Vue.component('f-' + item.toLowerCase(), require('./templates/Partials/Fields/' + item + '/' + item + '.vue').default);
	});

['logo1', 'logo2', 'ru-RU', 'en-US', 'vk', 'google', 'facebook']
	.map(item => {
		Vue.component('icon-' + item, require('./images/svg/' + item + '.svg'));
	});

Vue.mixin({
	methods: {
		setLanguage(language) {
			this.$store.commit('setLanguage', language);
		}
	}
});

let Application = new Vue({
	el: '#app',
	components: {
		Body: Body
	},
	template: '<Body/>',
	store,
	router,
	i18n
});

if (module.hot) {
	module.hot.accept(next => {
		let newDiv = document.createElement('div');
		newDiv.id = 'app';
		document.body.innerHTML = '';
		document.body.appendChild(newDiv);
	});
}

export default Application;