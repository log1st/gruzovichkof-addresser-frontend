'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';

import IndexPage from '../templates/Pages/Index/Index.vue';

Vue.use(VueRouter);

let router = new VueRouter({
	mode: 'hash',
	scrollBehavior(to, from, savedPosition) {
		return {x: 0, y: 0}
	},
	routes: [
		{
			path: '/',
			name: 'index',
			component: IndexPage
		}
	],
});

export default router;