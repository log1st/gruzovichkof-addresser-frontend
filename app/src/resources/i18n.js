'use strict';

import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Formatter from './i18n/formatter';
import messages from './i18n/messages/index';

Vue.use(VueI18n);

let formatter = new Formatter({
	locale: document.querySelector('html').dataset.lang
});

const i18n = new VueI18n({
	formatter,
	messages: messages,
	locale: 'en-US',
	fallbackLocale: 'en-US',
});

export default i18n;