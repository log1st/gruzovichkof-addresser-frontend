'use strict';

import Vue from 'vue';

import Vuex from 'vuex';

import messages from './i18n/messages/index';

import i18n from './i18n';

Vue.use(Vuex);

let store = new Vuex.Store({
	state: {
		xhrs: {},
		pageTitle: null,
		messages: messages,
		language: 'en-US',
		languages: [
			{
				key: 'ru-RU',
				label: 'Русский'
			},
			{
				key: 'en-US',
				label: 'English (USA)'
			}
		]
	},
	mutations: {
		setPageTitle(state, title) {
			state.pageTitle = title;
		},
		setLanguage(state, language) {
			state.language = language;
		}
	}
});

store.watch((state) => {
	return state.pageTitle;
}, (newValue) => {
	document.querySelector('head title').innerHTML = newValue;
});


store.watch((state) => {
	return state.language;
}, (newValue) => {
	i18n.locale = newValue;
});


export default store;