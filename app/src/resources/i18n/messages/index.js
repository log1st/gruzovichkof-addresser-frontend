'use strict';

import English from './_en';
import Russian from './_ru';

export default {
	'en-US': English,
	'ru-RU': Russian
}